using UnityEngine;

[ExecuteInEditMode]
public class ResizeToParent : MonoBehaviour {

    private void Start() {
        GameObject parent = transform.parent.gameObject;
        RectTransform rect = GetComponent<RectTransform>();

        // Set size to parent size
        rect.sizeDelta = parent.GetComponent<RectTransform>().sizeDelta;

        // Center in parent
        rect.localPosition = Vector2.zero;

        // Set width and height to 100%
        rect.anchorMin = Vector2.zero;
        rect.anchorMax = Vector2.one;
        rect.offsetMin = Vector2.zero;
        rect.offsetMax = Vector2.zero;

        Debug.LogWarning("Don't forget to remove this script. It has set the values and should be removed.");
    }

}

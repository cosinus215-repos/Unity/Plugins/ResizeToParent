# Resize To Parent
Simple script that resizes any `GameObject` on `Canvas`,
that has `RectTransform`, to its parent and keep it
the same size as parent `GameObject` at all times.

Note, that the script should be removed, as the values,
set by this script, persiste even after this script is removed.
